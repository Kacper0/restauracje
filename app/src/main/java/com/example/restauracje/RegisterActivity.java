package com.example.restauracje;

import android.content.AsyncQueryHandler;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.client.HttpResponseException;

public class RegisterActivity extends AppCompatActivity {

    Button submit;
    EditText tv_LoginR;
    EditText tv_PasswordR;
    EditText tv_Email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        tv_LoginR = findViewById(R.id.tv_LoginL);
        tv_PasswordR = findViewById(R.id.tv_PasswordL);
        tv_Email = findViewById(R.id.tv_Email);
        submit = findViewById(R.id.btn_SubmitL);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String loginR=tv_LoginR.getText().toString();
                final String passwordR=tv_PasswordR.getText().toString();
                final String email=tv_Email.getText().toString();
                Pattern pattern = Pattern.compile( "^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\\.([a-zA-Z])+([a-zA-Z])+");

                Matcher matcher = pattern.matcher(email);

                if(!loginR.isEmpty() && !passwordR.toString().isEmpty() &&!email.isEmpty()){
                    AsyncHttpClient client = new AsyncHttpClient();
                    final RequestParams params = new RequestParams();
                    params.put("login", tv_LoginR.getText().toString());
                    params.put("pass", tv_PasswordR.getText().toString());
                    params.put("email", tv_Email.getText().toString());
                    String url ="http://dev.imagit.pl/mobilne/api/register";

                    client.post(url,params,new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            String response= new String(responseBody);
                            if (response.equals("OK"))
                            {
                                Toast.makeText(RegisterActivity.this, RegisterActivity.this.getResources().getString(R.string.success),
                                        Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                                startActivity(intent);
                            }
                            else
                            {
                                Toast.makeText(RegisterActivity.this,R.string.error,
                                        Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            String response= new String(responseBody);

                            Toast.makeText(RegisterActivity.this,R.string.success,
                                    Toast.LENGTH_SHORT).show();
                        }
                    });
                    Intent intent = new Intent(RegisterActivity.this,LoginActivity.class);
                    startActivity(intent);
                }

                else{
                    Toast.makeText(RegisterActivity.this,R.string.error,Toast.LENGTH_SHORT).show();
                }




            }
        });
    }
}
