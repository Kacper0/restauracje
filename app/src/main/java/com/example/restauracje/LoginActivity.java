package com.example.restauracje;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import cz.msebera.android.httpclient.Header;

public class LoginActivity extends AppCompatActivity {

    Button btn_Submit;
    TextView tv_LoginL;
    TextView tv_PasswordL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        TextView tv_Register = findViewById(R.id.tv_Register);
        tv_LoginL=findViewById(R.id.tv_LoginL);
        tv_PasswordL = findViewById(R.id.tv_PasswordL);
        btn_Submit = (Button) findViewById(R.id.btn_SubmitL);
        final SharedPreferences preferences = getSharedPreferences("userP", Activity.MODE_PRIVATE);

        final String SavedLogin = preferences.getString("login","");
        final String SavedPassword = preferences.getString("password","");

        tv_LoginL.setText(SavedLogin);
        tv_PasswordL.setText(SavedPassword);

        final AsyncHttpClient client = new AsyncHttpClient();

        if(!SavedLogin.isEmpty() && !SavedPassword.isEmpty()) {

        client.get("http://dev.imagit.pl/mobilne/api/login/" + SavedLogin + "/" + SavedPassword, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                String response = new String(responseBody);

                if(android.text.TextUtils.isDigitsOnly(response)){
                    Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                    startActivity(intent);
                    }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
        }

        btn_Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String l=tv_LoginL.getText().toString();
                final String p=tv_PasswordL.getText().toString();

                if(!l.isEmpty() && !p.isEmpty()) {

                    String url = "http://dev.imagit.pl/mobilne/api/login/" + l + "/" + p;
                    client.get(url, new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            String response = new String(responseBody);
                            if(android.text.TextUtils.isDigitsOnly(response)){
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putString("login",l);
                                editor.putString("password",p);
                                editor.putString("user_id",response);
                                editor.commit();
                                Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                                startActivity(intent);
                            }
                            else
                            {
                                Toast.makeText(LoginActivity.this,R.string.error_login,Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        }
                    });

                }

            }
        });

    }

    public void TvRegister(View v){
        Intent intent = new Intent(LoginActivity.this,RegisterActivity.class);
        startActivity(intent);
    }


}
