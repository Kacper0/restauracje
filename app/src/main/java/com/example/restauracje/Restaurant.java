package com.example.restauracje;

public class Restaurant {

    String name;
    Double latitude;
    Double longitude;

    public Restaurant(String name, double latitude, double longitude){
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getName(){
        return name;
    }

    public Double getLatitude(){
        return  latitude;
    }

    public Double getLongitude(){
        return longitude;
    }
}
