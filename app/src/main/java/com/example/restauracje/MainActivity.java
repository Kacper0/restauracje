package com.example.restauracje;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import cz.msebera.android.httpclient.Header;


public class MainActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    private LocationManager mLocationManager;

    private Location location;

    private AsyncHttpClient client;

    private String userID;
    private String url;

    private boolean isGpsEnabled = false;
    private boolean isNetworkEnabled = false;


    ArrayList<String> restaurantsList;

    Button btn_Logout;
    Button btn_Add;


    ListView lv_Restaurants;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        btn_Logout = findViewById(R.id.btn_Logout);
        btn_Add = findViewById(R.id.btn_Add);

        lv_Restaurants = findViewById(R.id.lv_Restaurants);

        restaurantsList = new ArrayList<>();

        final SharedPreferences preferences = getSharedPreferences("userP", Activity.MODE_PRIVATE);
        userID = preferences.getString("user_id","0");

        final ArrayAdapter<String> restaurantsAdapter = new ArrayAdapter<String>(MainActivity.this,android.R.layout.simple_expandable_list_item_1,restaurantsList);
        lv_Restaurants.setAdapter(restaurantsAdapter);



        client = new AsyncHttpClient();

        url = "http://dev.imagit.pl/mobilne/api/restaurants/"+userID;

        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String JSON = new String(responseBody);
                JSONArray jArray = null;

                try {
                    jArray = new JSONArray(JSON);
                    for(int i=0; i<jArray.length(); i++)
                    {
                        JSONObject jObject = jArray.getJSONObject(i);
                        String restaurantName=jObject.getString("RESTAURANT_NAME");
                        String restaurantPhone=jObject.getString("RESTAURANT_PHONE");


                        restaurantsList.add(restaurantName+", "+restaurantPhone);
                    }

                    lv_Restaurants.setAdapter(restaurantsAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) { }
        });


        btn_Logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences.Editor editor = preferences.edit();

                editor.putString("login","");
                editor.putString("password","");
                editor.commit();

                Intent intent = new Intent(MainActivity.this,LoginActivity.class);
                startActivity(intent);

            }
        });

        btn_Add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AddNew.class);
                startActivity(intent);

            }
        });


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        try {
            mMap = googleMap;

            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            mMap.getUiSettings().setZoomControlsEnabled(true);


            location = MyLocation();

            LatLng position = new LatLng(location.getLatitude(), location.getLongitude());

            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(position, 10));


            client = new AsyncHttpClient();

            url = "http://dev.imagit.pl/mobilne/api/restaurants/" + userID;

            client.get(url, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    String JSON = new String(responseBody);
                    JSONArray jArray = null;

                    try {
                        jArray = new JSONArray(JSON);
                        for (int i = 0; i < jArray.length(); i++) {
                            JSONObject jObject = jArray.getJSONObject(i);
                            String restaurantName = jObject.getString("RESTAURANT_NAME");
                            String restarantlatitude = jObject.getString("RESTAURANT_LAT");
                            String restaurantlongitude = jObject.getString("RESTAURANT_LONG");

                            if (restarantlatitude.length() > 1 && restaurantlongitude.length() > 1) {

                                LatLng restaurantPosition = new LatLng(Double.parseDouble(restarantlatitude), Double.parseDouble(restaurantlongitude));
                                mMap.addMarker(new MarkerOptions().position(restaurantPosition).title(restaurantName));
                            }
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                }
            });
        }
        catch (Exception e ){}

    }

    public void btnRefresh(View view){
        onMapReady(mMap);
    }

    public Location MyLocation()
    {
        Location myLocation = null;
        try
        {
            mLocationManager = (LocationManager)getSystemService(MainActivity.this.LOCATION_SERVICE);

            isGpsEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            isNetworkEnabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if(!isGpsEnabled && !isNetworkEnabled)
            {
                buildAlertMessageNoGps();
            }
            if(isGpsEnabled)
            {
                myLocation = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);


            }
            if(isNetworkEnabled)
            {
                myLocation = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }

        }
        catch (Exception e){}

        return myLocation;
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }




}
